from django.contrib.auth.models import User, Group
from ldapauth.utils import get_user_groups
from django.conf import settings
from django.db.models.signals import post_migrate

class StaffMiddleware(object):
    """
    Sets the is_staff field on the request's user
    """

    def process_request(self, request):
        try:
            user = User.objects.get(id=request.user.id)
        except:
            return

        # group names are common for LDAP groups and Django model groups
        groups = Group.objects.values_list('name', flat=True)
        user_groups = list(set(get_user_groups(user.username)).intersection(set(groups)))

        if user_groups:
            current_groups = [g.name for g in user.groups.all()]
            rm_groups = Group.objects.filter(name__in=list(set(current_groups).difference(set(user_groups))))
            dj_groups = Group.objects.filter(name__in=user_groups)

            for dj in dj_groups:
                user.groups.add(dj)

            for rm in rm_groups:
                user.groups.remove(rm)

            if not user.is_staff:
                request.user.is_staff = True
                user.is_staff = True

            if settings.LADP_AUTH_SUPERUSER_GROUP in user_groups:
                request.user.is_superuser = True
                user.is_superuser = True
            elif user.is_superuser:
                request.user.is_superuser = False
                user.is_superuser = False

            user.save()
        else:
            if user.is_staff:
                request.user.is_staff = False
                user.is_staff = False
                user.groups.clear()
                user.save()


class SuperuserMiddleware(object):
    """
    Lets any superuser login as any other user.
    """

    def process_request(self, request):
        if request.user.is_superuser:
            if "doppelganger" in request.GET:
                username = request.GET.get("doppelganger")

                user = None
                try:
                    user = User.objects.get(username=username)
                    # TODO: Check for first_name, last_name, and email and set
                    # them if they don't exist.
                except User.DoesNotExist:
                    pass
                if user is not None:
                    # Store pk because user object is not JSON serializable
                    request.session["doppelganger"] = user.pk
                    if "student" in request.session:
                        del request.session["student"]

            if "doppelganger" in request.session:
                # The clear flag allows the current user to return to their
                # account by removing the doppelganger instance from the
                # session.
                if "_clear" in request.GET:
                    del request.session["doppelganger"]
                    if "student" in request.session:
                        del request.session["student"]
                else:
                    # Override request user with the doppelganger instance.
                    request.user = User.objects.get(pk=request.session["doppelganger"])


# if we are using the staff middleware then
# we need to seed the group table with the superuser ldap group
# This allows admins to login
def signal_receiver(sender, **kwargs):
    print(sender.name)
    if sender.name == 'django.contrib.auth':
        from django.contrib.auth.models import Group
        Group.objects.get_or_create(name=settings.LADP_AUTH_SUPERUSER_GROUP)
        print('perform initial data for group ')

if 'ldapauth.middleware.StaffMiddleware' in settings.MIDDLEWARE_CLASSES and settings.LADP_AUTH_SUPERUSER_GROUP:
    post_migrate.connect(signal_receiver)